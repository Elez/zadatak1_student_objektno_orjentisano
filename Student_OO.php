<?php 
	session_start();
?>

<!DOCTYPE html>
<html>
	<head>
		<title>Student_OO</title>
		<meta name="author" content="Elez"> 	
		<meta charset="UTF-8">
		<link rel="stylesheet" type="text/css" href="Student_OO.css" > 
		<!-- <style href="Student_OO.css">

			.form_student{
				text-align: center;
				margin-top: 50px;
				margin-left: 250px;
				margin-right: 250px;
				background-color: #66b3ff;
				border-radius: 20px;
				padding: 5px;
			}	

			.form2{
				text-align: center;
				margin-top: 10px;
				background-color: #66b3ff;
				margin-left: 250px;
				margin-right: 250px;
				border-radius: 20px;
				padding: 10px;				
			}
			.table{
				text-align: center;
				margin-top: 7px;
				margin-left: 30px;
				margin-right: 30px;
				padding-left: 35%;
			}
			.form_exam{
				margin-top: 20px;

			}


		</style> -->
	</head>
	<body>

		<!-- Wrapper begins --> 

		<div class="Wrapper">

			<!-- Form_student begins -->

			<div class="form_student">

				<?php  


					class Student
					{
						public $firstname;
						public $lastname;	
						public $index;
						public $town;
						public $exams = array();
						
						function __construct($firstname, $lastname, $index, $town)
						{
							$this->firstname = $firstname;
							$this->lastname = $lastname;
							$this->index = $index;
							$this->town = $town;
						}

						public function addExam($exam, $mark)
						{
							$this->exams[] = new Exam($exam, $mark);
						}
					}

					class Exam 
					{
						public $exam;
						public $mark;
						
						function __construct($exam, $mark)
						{
							$this->exam = $exam;
							$this->mark = $mark;
						}
					}
					$student = null;

					if(isset($_POST['firstname']) && isset($_POST['lastname']) && isset($_POST['index']) && isset($_POST['town'])) {

							$student = new Student($_POST['firstname'], $_POST['lastname'], $_POST['index'], $_POST['town']);

							$_SESSION['student'] = serialize($student);
					}

					if (isset($_SESSION['student'])) {
						$student = unserialize($_SESSION['student']);
						$firstname = $student->firstname;
						$lastname = $student->lastname;
						$index = $student->index;
						$town = $student->town;
					}

				?>

				<h1>Student</h1>

				<form method="post">

					Ime:<br>

					<input type="text" name="firstname" value= <?php if(isset($_SESSION['student'])) echo $firstname; ?>>
					
					<br>Prezime:<br>

				  	<input type="text" name="lastname" value=<?php if(isset($_SESSION['student'])) echo $lastname; ?>>
				  

					<br>Broj indeksa:<br>

				  	<input type="text" name="index" value=<?php if(isset($_SESSION['student']))echo $index; ?>>

					<br>Mesto:<br>

					<select name="town">
					
						<option name = "beograd" value="beograd"
								<?php if(isset($town) && isset($_POST['town']) && ($town == 'beograd')) echo "selected"; ?>
								>Beograd</option>
						<option name = "nis" value="nis"
								<?php if(isset($town) && isset($_POST['town']) && ($town == 'nis')) echo "selected"; ?>
								>Nis</option>
						<option name = "novi sad" value="novi sad"
								<?php if(isset($town) && isset($_POST['town']) && ($town == 'novi sad')) echo "selected"; ?>
								>Novi Sad</option>
					
					</select>
				    <br>
				    <br>
				  <input type="submit" value="Sacuvaj" name="submituj">
				</form>

			</div>

			<!-- Form_student ends -->

			<?php // kreiranje reda za tabelu

				$row = null;
				if(isset($_SESSION['student'])){
					$student = unserialize($_SESSION['student']);
					if (isset($_POST['exam']) && isset($_POST['mark']) && (!empty($_POST['exam']) &&  !empty($_POST['mark']))) {
						$student->addExam($_POST['exam'], $_POST['mark']);
						$_SESSION['student'] = serialize($student);
					}
					$row = $student->exams;
				}

			?>

			<!-- form2 begins -->

			<div class="form2">

				<!-- Table begins -->

				<div class="table">
					<table id="myTable" border="1px solid black">
						<tr>
					    	<th>Br.</th>
					    	<th>Ispit</th>    
					    	<th>Ocena</th>				     
					  	</tr>
					  	<?php // pravljenje redova u tabeli

					  		for ($i=0; $i < count($row); $i++) { 
					  			$br = $i+1;
					  			echo "<tr><td> $br </td>";
					  			foreach ($row[$i] as $r) {
					  				echo "<td> $r </td>";
					  			}
					  			echo "</td>";
					  		}

					  	?>
					</table>
				</div>
				
				<!-- Table ends -->


				<!-- Form_exam begins -->

				<div class="form_exam">

					<form method="post">

						Ispit:<br>

						<input type="text" name = "exam" >

						<br>Ocena:<br>

						<input type="number" name = "mark" min = "6" max = "10" >

						<br><br>

						<input type="submit" value="sacuvaj">	

					</form>
				</div>
			
				<!-- Form_exam ends -->

			</div>	

			<!-- form2 ends -->			

		</div>	

		<!-- Wrapper ends --> 

	</body>
</html>


